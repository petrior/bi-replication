package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-mysql-org/go-mysql/canal"
	"github.com/go-mysql-org/go-mysql/mysql"
	"github.com/siddontang/go-log/log"
)

type MyEventHandler struct {
	canal.DummyEventHandler
}

func (h *MyEventHandler) OnRow(e *canal.RowsEvent) error {
	// log.Infof("%s %v\n", e.Action, e.Rows)
	if e.Table.Name == "users" {
		// Could just overwrite some values...
		e.Rows[0][0] = 1000
		e.Rows[0][1] = ""
	}
	// log.Infof("%s %v\n", e.Action, e.Rows)

	// Probably need to check columns since e.Rows is
	// just an array.
	// log.Infof("%s %v\n", e.Table.Columns)

	return nil
}

func (h *MyEventHandler) OnPosSynced(pos mysql.Position, set mysql.GTIDSet, force bool) error {
	// Persist position here for example to a file?
	// Should you do that after every event?
	// What is force?
	log.Infof("%v", pos)
	return nil
}

func (h *MyEventHandler) String() string {
	return "MyEventHandler"
}

func main() {
	cfg := canal.NewDefaultConfig()
	cfg.Addr = "mariadb:3306"
	cfg.User = "root"
	cfg.Password = "root"
	cfg.Flavor = "mariadb"

	// Could maybe fetch list of databases first and use that.
	// Not sure if mysqldump would work with over one thousand
	// db's listed though.
	cfg.Dump.Databases = []string{
		"diarium_1200",
	}

	// Exclude some tables
	// cfg.ExcludeTableRegex = []string{
	// 	"appointments_api_export_cache",
	// 	"appointments_export_cache",
	// }

	c, err := canal.NewCanal(cfg)
	if err != nil {
		log.Fatal(err)
	}

	gracefulShutdown := make(chan os.Signal, 1)
	signal.Notify(gracefulShutdown, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-gracefulShutdown
		log.Info("Shutting down...")
		c.Close()
	}()

	c.SetEventHandler(&MyEventHandler{})

	// Skip database dump and start replicating at position
	// pos := new(mysql.Position)
	// pos.Pos = 328
	// pos.Name = "mysql-bin.000002"
	// c.RunFrom(pos)

	c.Run()

	// Only way to wait until replication connection is killed?
	// Replication connection is killed using kill query.
	time.Sleep(2 * time.Second)
}
